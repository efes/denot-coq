Require Export Program Setoid.
Require Import Morphisms.

Definition pointwise {A B} (R : relation B) : relation (A -> B) :=
  fun f g => forall x, R (f x) (g x).
Definition simprod {A B} (RA : relation A) (RB : relation B) : relation (A * B) :=
  fun p₁ p₂ => RA (fst p₁) (fst p₂) /\ RB (snd p₁) (snd p₂).
Arguments pointwise {A B} _ _ _/.
Arguments simprod {A B} _ _ _ _/.
Arguments flip {A B C} _ _ _ /.

Require Import Arith.

Declare Scope dom_scope.

(** POSets as CCC *)

Class POSet (A : Type) :=
  mkPO
    { preo : relation A;
      Rpreo :> PreOrder preo
    }.

Notation "x ⊑ y" := (preo x y) (at level 70, no associativity) : dom_scope.

Section POSBase.
  Context {A} `{POSet A}.
  Local Open Scope dom_scope.

  Definition equiv : relation A := fun x y => preo x y /\ preo y x.

  Global Instance Equiv_equiv : Equivalence equiv.
  Proof.
    split.
    - intros x; split; reflexivity.
    - intros x y [LExy LEyx]; split; assumption.
    - intros x y z [LExy LEyx] [LEyz LEzy]; split; etransitivity; eassumption.
  Qed.

  Global Instance Sub_equiv_preo : subrelation equiv preo.
  Proof.
    intros x y [LE _]; assumption.
  Qed.

  Global Instance POSet_PO : PartialOrder equiv preo.
  Proof.
    intros x y; simpl; reflexivity.
  Qed.

  Record MFun {A B} `{POSet A} `{POSet B} :=
    MF
      { mfun  :> A -> B;
        monotone :> Proper (preo ++> preo) mfun
      }.

End POSBase.

Arguments mkPO {A} _.
Arguments MF {A B _ _}.
Arguments MFun A B {_ _}.

Notation "x ≡ y" := (equiv x y) (at level 70, no associativity) : dom_scope.
Notation "A ->ᵐ B" := (MFun A B) (at level 99, right associativity, B at level 200) : type_scope.

Section POSets.
  Context {A B} `{poA : POSet A} `{poB : POSet B}.
  Local Open Scope dom_scope.

  Definition DiscretePOS A : POSet A := mkPO eq _.
  Global Instance POSnats  : POSet nat := mkPO le _.
  Global Instance POSunit  : POSet ()  := DiscretePOS ().

  Global Program Instance POSProp : POSet Prop := mkPO impl _.
  Next Obligation.
    split; intros p; unfold impl; tauto.
  Defined.

  Global Program Instance POSprod : POSet (A * B) :=
    mkPO (simprod preo preo) _.
  Next Obligation.
    split.
    - intros [x y]; split; reflexivity.
    - intros [x₁ y₁] [x₂ y₂] [x₃ y₃] [LEx₁ LEy₁] [LEx₂ LEy₂]; simpl.
      split; etransitivity; eassumption.
  Qed.

  Global Program Instance POSet_monotone : POSet (A ->ᵐ B) :=
    mkPO (pointwise preo) _.
  Next Obligation.
    split.
    - intros f x; reflexivity.
    - intros f g h LEfg LEgh x; etransitivity; [apply LEfg | apply LEgh].
  Qed.

  Global Program Instance SubsetPOS (P : A -> Prop) : POSet (sig P) :=
    mkPO (fun p₁ p₂ => ` p₁ ⊑ ` p₂) _.
  Next Obligation.
    split.
    - intros [x Px]; simpl; reflexivity.
    - intros [x Px] [y Py] [z Pz] LExy LEyz; simpl in *; etransitivity; eassumption.
  Qed.

  Global Instance monotone_mfun : Proper (preo ++> preo ++> preo) (mfun (A := A) (B := B)).
  Proof.
    intros f₁ f₂ LEf x₁ x₂ LEx.
    etransitivity; [apply LEf | apply monotone, LEx].
  Qed.

  Global Instance equiv_mfun : Proper (equiv ==> equiv ==> equiv) (mfun (A := A) (B := B)).
  Proof.
    intros f₁ f₂ EQf x₁ x₂ EQx; split; [rewrite EQf, EQx | rewrite <- EQf, <- EQx]; reflexivity.
  Qed.

  Global Instance monotone_pair : Proper (preo ==> preo ==> preo) pair.
  Proof.
    intros x₁ x₂ LEx y₁ y₂ LEy; split; assumption.
  Qed.

  Global Instance equiv_pair : Proper (equiv ==> equiv ==> equiv) pair.
  Proof.
    intros x₁ x₂ LEx y₁ y₂ LEy; split; split; simpl; apply LEx || apply LEy.
  Qed.

  (* since equiv is defined as sym closure, this is a lemma rather than def'n *)
  Lemma eqextᵐ (f g : A ->ᵐ B) (HExt : forall x, f x ≡ g x) : f ≡ g.
  Proof.
    split; intros x; apply HExt.
  Qed.

End POSets.

Section POSetOps.
  Context {A B C D} `{poA : POSet A} `{poB : POSet B} `{poC : POSet C} `{poD : POSet D}.
  Local Open Scope dom_scope.

  Program Definition compᵐ (f : B ->ᵐ C) (g : A ->ᵐ B) : A ->ᵐ C :=
    MF (fun x => f (g x)) _.
  Next Obligation.
    intros x y LE; apply monotone, monotone, LE.
  Qed.

  Program Definition idᵐ : A ->ᵐ A := MF (fun x => x) _.
  Next Obligation.
    intros x y LE; assumption.
  Qed.

  Program Definition constᵐ (x : B) : A ->ᵐ B := MF (fun _ => x) _.
  Next Obligation.
    intros _ _ _; reflexivity.
  Qed.

  Program Definition π₁ᵐ : A * B ->ᵐ A :=
    MF fst _.
  Next Obligation.
    intros [x₁ y₁] [x₂ y₂] [LEx _]; simpl; assumption.
  Qed.

  Program Definition π₂ᵐ : A * B ->ᵐ B :=
    MF snd _.
  Next Obligation.
    intros [x₁ y₁] [x₂ y₂] [_ LEy]; simpl; assumption.
  Qed.

  Program Definition medᵐ (f : C ->ᵐ A) (g : C ->ᵐ B) : C ->ᵐ A * B :=
    MF (fun x => (f x, g x)) _.
  Next Obligation.
    intros x y LExy; simpl; split; apply monotone, LExy.
  Qed.

  Program Definition πᵐ {P : A -> Prop} : sig P ->ᵐ A :=
    MF (proj1_sig (P := P)) _.
  Next Obligation.
    intros [x Px] [y Py] LE; apply LE.
  Qed.

  Program Definition curryᵐ (f : A * B ->ᵐ C) : A ->ᵐ B ->ᵐ C :=
    MF (fun x => MF (fun y => f (x, y)) _) _.
  Next Obligation.
    intros y₁ y₂ LEy; rewrite LEy; reflexivity.
  Qed.
  Next Obligation.
    intros x₁ x₂ LEx y; simpl; rewrite LEx; reflexivity.
  Qed.

  Program Definition evᵐ : (A ->ᵐ B) * A ->ᵐ B :=
    MF (fun p => fst p (snd p)) _.
  Next Obligation.
    intros [f₁ x₁] [f₂ x₂] [LEf LEx]; simpl in *.
    rewrite LEx; apply LEf.
  Qed.

  Global Instance Proper_compᵐ_ord : Proper (preo ++> preo ++> preo) compᵐ.
  Proof.
    intros f₁ f₂ LEf g₁ g₂ LEg x; simpl.
    rewrite LEf, LEg; reflexivity.
  Qed.

  Global Instance Proper_compᵐ_eq : Proper (equiv ==> equiv ==> equiv) compᵐ.
  Proof.
    intros f₁ f₂ EQf g₁ g₂ EQg; apply eqextᵐ; intros x; simpl.
    rewrite EQf, EQg; reflexivity.
  Qed.

  Global Instance Proper_medᵐ_ord : Proper (preo ++> preo ++> preo) medᵐ.
  Proof.
    intros f₁ f₂ LEf g₁ g₂ LEg; split; simpl; [apply LEf | apply LEg].
  Qed.

  Global Instance Proper_medᵐ_eq : Proper (equiv ==> equiv ==> equiv) medᵐ.
  Proof.
    intros f₁ f₂ EQf g₁ g₂ EQg; split; (split; [apply EQf | apply EQg]).
  Qed.

  Global Instance Proper_curryᵐ_ord : Proper (preo ++> preo) curryᵐ.
  Proof.
    intros f₁ f₂ LEf x y; simpl; apply LEf.
  Qed.

  Global Instance Proper_curryᵐ_eq : Proper (equiv ==> equiv) curryᵐ.
  Proof.
    intros f₁ f₂ EQf; apply eqextᵐ; intros x; apply eqextᵐ; intros y; simpl; rewrite EQf; reflexivity.
  Qed.

End POSetOps.

Arguments idᵐ A {poA}.
Arguments evᵐ A B {poA poB}.
Notation "f ∘ᵐ g" := (compᵐ f g) (at level 40, left associativity) : dom_scope.
Notation "⟨ f , g ⟩ᵐ" := (medᵐ f g) (at level 0, f, g at level 69) : dom_scope.

Delimit Scope dom_scope with dom.

Definition prodᵐ {A B C D} `{POSet A} `{POSet B} `{POSet C} `{POSet D}
           (f : A ->ᵐ B) (g : C ->ᵐ D) : A * C ->ᵐ B * D := ⟨f ∘ᵐ π₁ᵐ, g ∘ᵐ π₂ᵐ⟩ᵐ%dom.

Notation "f ×ᵐ g" := (prodᵐ f g) (at level 40, left associativity) : dom_scope.

Instance monotone_prodᵐ  {A B C D} `{POSet A} `{POSet B} `{POSet C} `{POSet D} :
  Proper (preo ++> preo ++> preo) (prodᵐ (A := A) (B := B) (C := C) (D := D)).
Proof.
  intros f₁ f₂ LEf g₁ g₂ LEg; unfold prodᵐ; now rewrite LEf, LEg.
Qed.
Instance equiv_prodᵐ {A B C D} `{POSet A} `{POSet B} `{POSet C} `{POSet D} :
  Proper (equiv ++> equiv ++> equiv) (prodᵐ (A := A) (B := B) (C := C) (D := D)).
Proof.
  intros f₁ f₂ LEf g₁ g₂ LEg; unfold prodᵐ; now rewrite LEf, LEg.
Qed.

Section POSetDerived.
  Context {A B C} `{POSet A} `{POSet B} `{POSet C}.
  Local Open Scope dom_scope.

  Definition postcompᵐ (f : B ->ᵐ C) : (A ->ᵐ B) ->ᵐ (A ->ᵐ C) :=
    curryᵐ (f ∘ᵐ evᵐ A B).
  Definition precompᵐ  (f : B ->ᵐ C) : (C ->ᵐ A) ->ᵐ (B ->ᵐ A) :=
    curryᵐ (evᵐ C A ∘ᵐ (idᵐ (C ->ᵐ A) ×ᵐ f)).

  Local Obligation Tactic := intros.
  Program Definition uncurryᵐ {A B C} `{POSet A} `{POSet B} `{POSet C} :
    (A ->ᵐ B ->ᵐ C) ->ᵐ A * B ->ᵐ C := MF (fun f => evᵐ B C ∘ᵐ (f ×ᵐ idᵐ B)) _.
  Next Obligation.
    intros f₁ f₂ LEf; now rewrite LEf.
  Qed.
End POSetDerived.

Section POSetProps.
  Context {A B C D} `{poA : POSet A} `{poB : POSet B} `{poC : POSet C} `{poD : POSet D}.
  Local Open Scope dom_scope.

  (** POSets w/ monotone maps form a category *)
  Lemma compᵐ_assoc (f : C ->ᵐ D) (g : B ->ᵐ C) (h : A ->ᵐ B) :
    f ∘ᵐ (g ∘ᵐ h) ≡ f ∘ᵐ g ∘ᵐ h.
  Proof.
    apply eqextᵐ; intros x; simpl; reflexivity.
  Qed.
  Lemma compidᵐR (f : A ->ᵐ B) :
    f ∘ᵐ idᵐ A ≡ f.
  Proof.
    apply eqextᵐ; intros x; simpl; reflexivity.
  Qed.
  Lemma compidᵐL (f : A ->ᵐ B) :
    idᵐ B ∘ᵐ f ≡ f.
  Proof.
    apply eqextᵐ; intros x; simpl; reflexivity.
  Qed.

  (** () is terminal *)
  Definition uniqᵐ : A ->ᵐ () := constᵐ ().

  Lemma uniq_termᵐ (f g : A ->ᵐ ()) : f ≡ g.
  Proof.
    apply eqextᵐ; intros x; destruct (f x); destruct (g x); reflexivity.
  Qed.

  (** A * B is a product *)
  Lemma medᵐL (f : C ->ᵐ A) (g : C ->ᵐ B) :
    π₁ᵐ ∘ᵐ ⟨f, g⟩ᵐ ≡ f.
  Proof.
    apply eqextᵐ; intros; simpl; reflexivity.
  Qed.
  Lemma medᵐR (f : C ->ᵐ A) (g : C ->ᵐ B) :
     π₂ᵐ ∘ᵐ ⟨f, g⟩ᵐ ≡ g.
  Proof.
    apply eqextᵐ; intros; simpl; reflexivity.
  Qed.
  Lemma medᵐI : ⟨π₁ᵐ, π₂ᵐ⟩ᵐ ≡ idᵐ (A * B).
  Proof.
    apply eqextᵐ; intros [a b]; simpl; reflexivity.
  Qed.

  (** A ->ᵐ B is an exponential *)
  Lemma curry_evᵐ (f : A * B ->ᵐ C) :
    evᵐ B C ∘ᵐ (curryᵐ f ×ᵐ idᵐ B) ≡ f.
  Proof.
    apply eqextᵐ; intros [x y]; simpl; reflexivity.
  Qed.

End POSetProps.


(** ωCPOS as CCC *)

Section CPOBase.
  Context {A} `{poA : POSet A}.
  Local Open Scope dom_scope.

  Definition ωChain := nat ->ᵐ A.

  Class ωCPO :=
    mkCPO
      { compl : ωChain -> A;
        compl_ub  : forall (c : ωChain) n, c n ⊑ compl c;
        compl_lub : forall c x, c ⊑ constᵐ x -> compl c ⊑ x
      }.

End CPOBase.

Arguments ωChain A {poA}.
Arguments ωCPO A {poA}.
Notation "c ⊑ᵖ x" := (c ⊑ constᵐ x)%dom (at level 70, no associativity) : dom_scope.
Notation "⊔" := compl : dom_scope.

Section CPOInst.
  Context {A B} `{cpoA : ωCPO A} `{cpoB : ωCPO B}.
  Local Open Scope dom_scope.

  Definition Continuous (f : A ->ᵐ B) :=
    forall c : ωChain A, f (⊔ c) ⊑ ⊔ (f ∘ᵐ c).

  Record CFun :=
    CF
      { cfun :> A ->ᵐ B;
        continuous :> Continuous cfun
      }.

  Program Definition DiscreteωCPO A : ωCPO A := mkCPO (poA := DiscretePOS A) (fun c => c 0) _ _.
  Next Obligation.
    assert (LE : 0 ⊑ n) by apply le_0_n.
    now rewrite LE.
  Qed.
  Global Program Instance UnitωCPO : ωCPO () := mkCPO (fun _ => ()) _ _.
  Next Obligation.
    now destruct (c n).
  Qed.
  Next Obligation.
    now destruct x.
  Qed.

  Global Program Instance PropωCPO : ωCPO Prop :=
    mkCPO (fun c : ωChain Prop => exists n, c n) _ _.
  Next Obligation.
    intros Hcn; exists n; assumption.
  Qed.
  Next Obligation.
    intros [n Hcn]; apply H with n, Hcn.
  Defined.

  Global Program Instance ProdωCPO : ωCPO (A * B) :=
    mkCPO (fun c => (⊔ (π₁ᵐ ∘ᵐ c), ⊔ (π₂ᵐ ∘ᵐ c))) _ _.
  Next Obligation.
    split.
    - apply (compl_ub (π₁ᵐ ∘ᵐ c)).
    - apply (compl_ub (π₂ᵐ ∘ᵐ c)).
  Qed.
  Next Obligation.
    split; apply compl_lub; intros x; apply H.
  Qed.

  Global Program Instance CFunPOSet : POSet (CFun) :=
    mkPO (pointwise preo) _.
  Next Obligation.
    split.
    - intros f x; reflexivity.
    - intros f g h LEfg LEgh x; etransitivity; [apply LEfg | apply LEgh].
  Qed.

  Global Program Instance CFunωCPO : ωCPO CFun :=
    mkCPO (fun c => CF (MF (fun x => compl (MF (fun n => c n x) _)) _) _) _ _.
  Next Obligation.
    intros n₁ n₂ LEn; apply c, LEn.
  Qed.
  Next Obligation.
    intros x₁ x₂ LEx.
    apply compl_lub; intros n; etransitivity; [| apply compl_ub]; simpl; now rewrite LEx.
  Qed.
  Next Obligation.
    intros ca. simpl.
    apply compl_lub; intros n; simpl.
    rewrite (continuous (c n)); apply compl_lub; intros m; simpl.
    rewrite <- compl_ub; simpl; rewrite <- compl_ub; simpl; reflexivity.
  Qed.
  Next Obligation.
    rewrite <- compl_ub; simpl; reflexivity.
  Qed.
  Next Obligation.
    apply compl_lub; intros n; simpl; now apply H.
  Qed.

  Global Instance Proper_preo_compl : Proper (preo ++> preo) (compl (A := A)).
  Proof.
    intros c₁ c₂ LEc.
    apply compl_lub; intros n; simpl.
    etransitivity; [| apply compl_ub]; apply LEc.
  Qed.

  Global Instance Proper_equiv_compl : Proper (equiv ==> equiv) (compl (A := A)).
  Proof.
    intros c₁ c₂ EQc; split; [rewrite EQc | rewrite <- EQc]; reflexivity.
  Qed.

  Global Instance Proper_preo_cfun : Proper (preo ++> preo) cfun.
  Proof.
    intros f₁ f₂ LEf x; apply LEf.
  Qed.

  Global Instance Proper_equiv_cfun : Proper (equiv ==> equiv) cfun.
  Proof.
    intros f₁ f₂ LEf; apply LEf.
  Qed.

  Lemma compl_const (x : A) : ⊔ (constᵐ x) ≡ x.
  Proof.
    split; [| rewrite <- compl_ub with (n := 0); reflexivity].
    apply compl_lub; intros n; simpl; reflexivity.
  Qed.

End CPOInst.

Arguments CF {A B _ _ _ _}.
Arguments CFun A B {_ _ _ _}.


Notation "A ->ᶜ B" := (CFun A B) (at level 99, right associativity, B at level 200) : type_scope.

Section CPODefs.
  Context {A B C} `{ωCPO A} `{ωCPO B} `{ωCPO C}.
  Local Open Scope dom_scope.

  Program Definition compᶜ (f : B ->ᶜ C) (g : A ->ᶜ B) : A ->ᶜ C :=
    CF (f ∘ᵐ g) _.
  Next Obligation.
    intros c; simpl; rewrite !continuous, compᵐ_assoc; reflexivity.
  Qed.

  Program Definition idᶜ : A ->ᶜ A := CF (idᵐ A) _.
  Next Obligation.
    intros c; simpl; apply Proper_preo_compl; intros x; reflexivity.
  Qed.

  Program Definition constᶜ x : A ->ᶜ B := CF (constᵐ x) _.
  Next Obligation.
    intros c; simpl; rewrite <- compl_ub with (n := 0); reflexivity.
  Qed.

  Program Definition π₁ᶜ : A * B ->ᶜ A :=
    CF π₁ᵐ _.
  Next Obligation.
    intros c; simpl; reflexivity.
  Qed.

  Program Definition π₂ᶜ : A * B ->ᶜ B :=
    CF π₂ᵐ _.
  Next Obligation.
    intros c; simpl; reflexivity.
  Qed.

  Program Definition medᶜ (f : C ->ᶜ A) (g : C ->ᶜ B) : C ->ᶜ A * B :=
    CF (medᵐ f g) _.
  Next Obligation.
    intros c; simpl; split.
    - rewrite compᵐ_assoc, medᵐL, continuous; reflexivity.
    - rewrite compᵐ_assoc, medᵐR, continuous; reflexivity.
  Qed.

  Program Definition curryᶜ (f : A * B ->ᶜ C) : A ->ᶜ B ->ᶜ C :=
    CF (MF (fun x => CF (curryᵐ f x) _) _) _.
  Next Obligation.
    intros c; simpl.
    assert (HT := continuous f ⟨constᵐ x, c⟩ᵐ); simpl in HT.
    rewrite medᵐL, compl_const, medᵐR in HT; simpl in HT.
    rewrite HT; clear HT; apply Proper_preo_compl; intros n; simpl; reflexivity.
  Qed.
  Next Obligation.
    intros x₁ x₂ LEx y; simpl; rewrite LEx; reflexivity.
  Qed.
  Next Obligation.
    intros c y; simpl.
    assert (HT := continuous f ⟨c, constᵐ y⟩ᵐ); simpl in HT.
    rewrite medᵐR, medᵐL, compl_const in HT; simpl in HT.
    rewrite HT; clear HT; apply Proper_preo_compl; intros n; simpl; reflexivity.
  Qed.

  Definition Uᵐ : (A ->ᶜ B) ->ᵐ (A ->ᵐ B) := MF cfun _.

  Program Definition evᶜ : (A ->ᶜ B) * A ->ᶜ B :=
    CF (evᵐ A B ∘ᵐ (Uᵐ ×ᵐ idᵐ A)) _.
  Next Obligation.
    intros c; simpl; apply compl_lub; intros n; simpl.
    rewrite continuous; apply compl_lub; intros k; simpl.
    rewrite <- compl_ub with (n0 := max n k); simpl.
    assert (HCn : c n ⊑ c (max n k)) by (apply monotone, Nat.le_max_l).
    assert (HCk : c k ⊑ c (max n k)) by (apply monotone, Nat.le_max_r).
    destruct HCn as [HF _]; destruct HCk as [_ HV].
    etransitivity; [apply HF | apply monotone, HV].
  Qed.

  Global Instance Proper_compᶜ_ord : Proper (preo ++> preo ++> preo) compᶜ.
  Proof.
    intros f₁ f₂ LEf g₁ g₂ LEg x; simpl.
    rewrite LEf, LEg; reflexivity.
  Qed.

  Global Instance Proper_compᶜ_eq : Proper (equiv ==> equiv ==> equiv) compᶜ.
  Proof.
    intros f₁ f₂ EQf g₁ g₂ EQg; apply eqextᵐ; intros x; simpl.
    rewrite EQf, EQg; reflexivity.
  Qed.

  Global Instance Proper_medᶜ_ord : Proper (preo ++> preo ++> preo) medᶜ.
  Proof.
    intros f₁ f₂ LEf g₁ g₂ LEg; split; simpl; [apply LEf | apply LEg].
  Qed.

  Global Instance Proper_medᶜ_eq : Proper (equiv ==> equiv ==> equiv) medᶜ.
  Proof.
    intros f₁ f₂ EQf g₁ g₂ EQg; split; (split; [apply EQf | apply EQg]).
  Qed.

  Global Instance Proper_curryᶜ_ord : Proper (preo ++> preo) curryᶜ.
  Proof.
    intros f₁ f₂ LEf x y; simpl; apply LEf.
  Qed.

  Global Instance Proper_curryᶜ_eq : Proper (equiv ==> equiv) curryᶜ.
  Proof.
    intros f₁ f₂ EQf; apply eqextᵐ; intros x; apply eqextᵐ; intros y; simpl; rewrite EQf; reflexivity.
  Qed.

End CPODefs.

Arguments idᶜ A {_ _}.
Arguments evᶜ A B {_ _ _ _}.
Notation "f ∘ᶜ g" := (compᶜ f g) (at level 40, left associativity) : dom_scope.
Notation "⟨ f , g ⟩ᶜ" := (medᶜ f g) (at level 0, f, g at level 69) : dom_scope.

Definition prodᶜ {A B C D} `{ωCPO A} `{ωCPO B} `{ωCPO C} `{ωCPO D}
           (f : A ->ᶜ B) (g : C ->ᶜ D) : A * C ->ᶜ B * D := ⟨f ∘ᶜ π₁ᶜ, g ∘ᶜ π₂ᶜ⟩ᶜ%dom.

Notation "f ×ᶜ g" := (prodᶜ f g) (at level 40, left associativity) : dom_scope.

Section CPOProps.
  Context {A B C D} `{cpoA : ωCPO A} `{cpoB : ωCPO B} `{cpoC : ωCPO C} `{cpoD : ωCPO D}.
  Local Open Scope dom_scope.

  (** CPOs w/ continuous maps form a category *)
  Lemma compᶜ_assoc (f : C ->ᶜ D) (g : B ->ᶜ C) (h : A ->ᶜ B) :
    f ∘ᶜ (g ∘ᶜ h) ≡ f ∘ᶜ g ∘ᶜ h.
  Proof.
    apply eqextᵐ; intros x; simpl; reflexivity.
  Qed.
  Lemma compidᶜR (f : A ->ᶜ B) :
    f ∘ᶜ idᶜ A ≡ f.
  Proof.
    apply eqextᵐ; intros x; simpl; reflexivity.
  Qed.
  Lemma compidᶜL (f : A ->ᶜ B) :
    idᶜ B ∘ᶜ f ≡ f.
  Proof.
    apply eqextᵐ; intros x; simpl; reflexivity.
  Qed.

  (** () is terminal *)
  Definition uniqᶜ : A ->ᶜ () := constᶜ ().

  Lemma uniq_termᶜ (f g : A ->ᶜ ()) : f ≡ g.
  Proof.
    apply eqextᵐ; intros x; destruct (f x); destruct (g x); reflexivity.
  Qed.

  (** A * B is a product *)
  Lemma medᶜL (f : C ->ᶜ A) (g : C ->ᶜ B) :
    π₁ᶜ ∘ᶜ ⟨f, g⟩ᶜ ≡ f.
  Proof.
    apply eqextᵐ; intros; simpl; reflexivity.
  Qed.
  Lemma medᶜR (f : C ->ᶜ A) (g : C ->ᶜ B) :
     π₂ᶜ ∘ᶜ ⟨f, g⟩ᶜ ≡ g.
  Proof.
    apply eqextᵐ; intros; simpl; reflexivity.
  Qed.
  Lemma medᶜI : ⟨π₁ᶜ, π₂ᶜ⟩ᶜ ≡ idᶜ (A * B).
  Proof.
    apply eqextᵐ; intros [a b]; simpl; reflexivity.
  Qed.

  (** A ->ᶜ B is an exponential *)
  Lemma curry_evᶜ (f : A * B ->ᶜ C) :
    evᶜ B C ∘ᶜ (curryᶜ f ×ᶜ idᶜ B) ≡ f.
  Proof.
    apply eqextᵐ; intros [x y]; simpl; reflexivity.
  Qed.

  Definition postcompᶜ (f : B ->ᶜ C) : (A ->ᶜ B) ->ᶜ (A ->ᶜ C) := curryᶜ (f ∘ᶜ evᶜ A B).

  Definition precompᶜ (f : B ->ᶜ C) : (C ->ᶜ A) ->ᶜ (B ->ᶜ A) := curryᶜ (evᶜ C A ∘ᶜ (idᶜ (C ->ᶜ A) ×ᶜ f)).

End CPOProps.

Notation " f ▹ᶜ " := (postcompᶜ f) (at level 20) : dom_scope.
Notation " ◃ᶜ f " := (precompᶜ f) (at level 20) : dom_scope.

Class Admissible {X} `{ωCPO X} (P : X -> Prop) :=
  {adm : forall c : ωChain X, (forall n, P (c n)) -> P (⊔ c)%dom}.

Program Instance SubsetCPO (X : Set) (P : X -> Prop) `{Admissible X P} : ωCPO (sig P) :=
  mkCPO (fun c => exist _ (⊔ (πᵐ ∘ᵐ c))%dom _) _ _.
Next Obligation.
  apply adm; intros n; simpl; apply proj2_sig.
Qed.
Next Obligation.
  rewrite <- compl_ub; simpl; reflexivity.
Qed.
Next Obligation.
  apply compl_lub; intros n; apply H1.
Qed.

(** PCPOs and fixpoints *)
(** XXX: No need for A to be a CPO, enough to be a POSet, but maybe
         less pleasant to work with. *)
Class PCPO A `{cpoA : ωCPO A} :=
  mkPCPO
    { bot : A;
      botLeast : forall x, (bot ⊑ x)%dom
    }.

Arguments mkPCPO {_ _ _} _ _.
Notation "⊥" := bot : dom_scope.

Section PCPOs.
  Context {A B} `{pcpoA : PCPO A} `{pcpoB : PCPO B}.
  Local Open Scope dom_scope.

  Global Program Instance PtdUnit : PCPO () := mkPCPO () _.
  Next Obligation.
    destruct x; reflexivity.
  Qed.

  Global Program Instance PtdProp : PCPO Prop :=
    mkPCPO False _.
  Next Obligation.
    intros [].
  Defined.

  Global Program Instance PtdProd : PCPO (A * B) :=
    mkPCPO (⊥, ⊥) _.
  Next Obligation.
    split; apply botLeast.
  Qed.

  Global Program Instance PtdFun  : PCPO (A ->ᶜ B) :=
    mkPCPO (constᶜ ⊥) _.
  Next Obligation.
    apply botLeast.
  Qed.

  (* iter defined inside-out, so apprᵖ easier to define; reqs generalization in other places *)
  Fixpoint iter (f : A ->ᶜ A) n :=
    match n with
    | O   => idᶜ A
    | S n => iter f n ∘ᶜ f
    end.

  Lemma iterLR (f : A ->ᶜ A) n x :
    iter f n (f x) = f (iter f n x).
  Proof.
    revert x; induction n; intros; simpl; [reflexivity | apply IHn].
  Qed.

  Program Definition apprᵖ : (A ->ᶜ A) * nat ->ᵐ A :=
    MF (fun fn => iter (fst fn) (snd fn) ⊥) _.
  Next Obligation.
    intros [f m] [g n] [LEf LEn]; simpl in *; induction LEn.
    - induction m; simpl; [reflexivity |].
      rewrite !iterLR, IHm, LEf; reflexivity.
    - rewrite IHLEn; simpl; apply monotone, botLeast.
  Qed.

  Definition fixp (f : A ->ᶜ A) := ⊔ (curryᵐ apprᵖ f).

  Lemma fixp_ind : forall (f : A ->ᶜ A) (P : A -> Prop) {Adm : Admissible P},
      P ⊥ -> (forall x (IHx : P x), P (f x)) ->
      P (fixp f).
  Proof.
    intros ? ? ? Hbot Hstep; apply adm; intros n.
    induction n; simpl; [assumption |].
    rewrite iterLR; apply Hstep, IHn.
  Qed.

  Lemma fixp_unfold (f : A ->ᶜ A) :
    fixp f ≡ f (fixp f).
  Proof.
    unfold fixp; split.
    - eapply compl_lub; intros n; simpl.
      rewrite <- compl_ub with (n0 := n); simpl.
      rewrite <- iterLR; apply monotone, botLeast.
    - rewrite continuous.
      eapply compl_lub; intros n; simpl; rewrite <- compl_ub with (n0 := S n); simpl.
      rewrite iterLR; reflexivity.
  Qed.

  Program Definition fixpᵐ : (A ->ᶜ A) ->ᵐ A :=
    MF fixp _.
  Next Obligation.
    intros f₁ f₂ LEf; apply Proper_preo_compl; intros n; simpl.
    generalize (⊥ : A) as x; induction n; intros; [reflexivity | simpl].
    rewrite LEf; apply IHn.
  Qed.

  Lemma fixp_compl_diag (c : ωChain (A ->ᶜ A)) :
    fixp (⊔ c) ⊑ ⊔ (apprᵖ ∘ᵐ ⟨c, idᵐ nat⟩ᵐ).
  Proof.
    apply compl_lub; intros n.
    change (iter (⊔ c) n ⊥ ⊑ ⊔ (apprᵖ ∘ᵐ ⟨c, idᵐ nat⟩ᵐ)).
    induction n; [apply botLeast |].
    change (iter (⊔ c) (S n) ⊥) with (iter (⊔ c) n ((⊔ c) ⊥)); rewrite iterLR.
    rewrite IHn; clear n IHn; apply compl_lub; intros n; simpl.
    rewrite continuous; apply compl_lub; intros m; simpl.
    rewrite <- (compl_ub _ (S (max m n))); simpl; simpl; rewrite iterLR.
    assert (HT : c n ⊑ c (S (max m n))) by apply monotone, Nat.le_le_succ_r, Nat.le_max_r.
    rewrite HT; clear HT; apply monotone.
    assert (HT := monotone apprᵖ (c m, m) (c (S (max m n)), max m n)); apply HT; clear HT.
    split; simpl; [intros x |]; [| apply Nat.le_max_l].
    rewrite <- Nat.le_le_succ_r; [reflexivity | apply Nat.le_max_l].
  Qed.

  Lemma diag_compl_fix (c : ωChain (A ->ᶜ A)) :
    ⊔ (apprᵖ ∘ᵐ ⟨c, idᵐ nat⟩ᵐ) ⊑ ⊔ (fixpᵐ ∘ᵐ c).
  Proof.
    apply compl_lub; intros n; simpl.
    rewrite <- compl_ub; simpl; unfold fixp; rewrite <- compl_ub; simpl; reflexivity.
  Qed.

  Program Definition fixpᶜ : (A ->ᶜ A) ->ᶜ A :=
    CF fixpᵐ _.
  Next Obligation.
    intros c.
    rewrite fixp_compl_diag, diag_compl_fix; reflexivity.
  Qed.

  Lemma fixp_smaller (f : A->ᶜ A) (a : A) (AFix : f a ≡ a) :
    fixp f ⊑ a.
    unfold fixp. apply compl_lub. intros n. simpl.
    induction n; simpl.
    + apply botLeast.
    + rewrite iterLR. rewrite <- AFix. apply monotone. assumption.
  Qed.

End PCPOs.

Require Import Utf8.

Section Lifting.
  Context A `{cpoA : ωCPO A}.
  Local Open Scope dom_scope.

  CoInductive Stream := ε : Stream -> Stream | Val : A -> Stream.

  Inductive nth_tail : Stream -> nat -> Stream -> Prop :=
  | tail0 σ : nth_tail σ 0 σ
  | tailS σ₁ σ₂ n (NT : nth_tail σ₁ n σ₂) : nth_tail (ε σ₁) (S n) σ₂.

  CoInductive StrLE : Stream -> Stream -> Prop :=
  | SLEεε σ₁ σ₂ (LE : StrLE σ₁ σ₂)      : StrLE (ε σ₁) (ε σ₂)
  | SLEεv v  σ  (LE : StrLE σ  (Val v)) : StrLE (ε σ)  (Val v)
  | SLEvv v₁ v₂ n σ (NT : nth_tail σ n (Val v₂)) (LE : v₁ ⊑ v₂) : StrLE (Val v₁) σ.

  Lemma StrLE_coind : ∀ R : Stream -> Stream -> Prop,
      (∀ σ₁ σ₂, R (ε σ₁) (ε σ₂) -> R σ₁ σ₂) ->
      (∀ σ v, R (ε σ) (Val v) -> R σ (Val v)) ->
      (∀ v σ, R (Val v) σ -> ∃ n v', nth_tail σ n (Val v') /\ v ⊑ v') ->
      ∀ σ₁ σ₂, R σ₁ σ₂ -> StrLE σ₁ σ₂.
  Proof.
    cofix FOO; intros.
    destruct σ₁ as [σ₁ | v]; [destruct σ₂ as [σ₂ | v'] |].
    - apply SLEεε, FOO with R; auto.
    - apply SLEεv, FOO with R; auto.
    - destruct (H1 _ _ H2) as [n [v' [HNT HSub]]].
      eapply SLEvv; eassumption.
  Qed.

  Global Instance POStream : PreOrder StrLE.
  Proof.
    split.
    - intros x. apply StrLE_coind with (fun x y => x = y); [..| reflexivity].
      + intros ? ? HEq; now inversion HEq.
      + intros; discriminate.
      + intros ? ? HEq; subst; exists 0, v; split; [apply tail0 | reflexivity].
    - intros x y z LExy LEyz.
      apply StrLE_coind with (fun x z => exists y, StrLE x y /\ StrLE y z); [.. | eexists; split; eassumption]; clear x y z LExy LEyz.
      + intros x z [y [LExy LEyz]].
        inversion LExy; subst; [inversion LEyz; subst; eexists; split; eassumption |].
        eexists; split; [eassumption | inversion LEyz; subst; clear x LExy LEyz LE].
        inversion NT; subst; clear NT; eapply SLEvv; eassumption.
      + intros x z [y [LExy LEyz]].
        inversion LExy; subst; [inversion LEyz; subst; eexists; split; eassumption |].
        inversion LEyz; subst; inversion NT; subst; clear LEyz LExy.
        eexists; split; [eassumption |].
        eapply SLEvv; eassumption.
      + intros v σ₂ [σ₁ [LE₁ LE₂]].
        inversion LE₁; subst; clear LE₁.
        revert σ₁ σ₂ LE₂ NT; induction n; intros.
        * inversion NT; subst; inversion LE₂; subst; clear LE₂.
          do 2 eexists; split; [| etransitivity]; eassumption.
        * inversion NT; subst; clear NT.
          inversion LE₂; subst; clear LE₂.
          -- destruct (IHn _ _ LE0 NT0) as [m [v' [NT SUB]]].
             exists (S m), v'; split; [apply tailS |]; assumption.
          -- destruct (IHn _ _ LE0 NT0) as [m [v' [NT SUB]]].
             inversion NT; subst; clear NT NT0 LE0.
             exists 0, v'; split; [apply tail0 | assumption].
  Qed.

  Global Instance POSetStream : POSet Stream := mkPO StrLE _.

  (* TODO: Show Stream is a (P)CPO *)

End Lifting.
