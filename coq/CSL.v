Require Import Domains.
Require Import GenProd.
Require Import Pred.
Require Import List.
Require Import ZArith.BinInt.
Open Scope list_scope.
Arguments impl _ _ /.
Require Import Morphisms.

Declare Scope csl_scope.

Definition Env Tp := list Tp.

Parameter Agent : Set.
Parameter Resource : Set.
Parameter Time : Set.

Inductive BaseTp := TAgent | TResource | TTime | TInt | TBool.

Inductive BinOp : BaseTp -> BaseTp -> BaseTp -> Set :=
| AddZ : BinOp TInt TInt TInt
| SubZ : BinOp TInt TInt TInt
| MulZ : BinOp TInt TInt TInt
| DivZ : BinOp TInt TInt TInt
| AndB : BinOp TBool TBool TBool
| OrB : BinOp TBool TBool TBool
| Eq : BinOp TInt TInt TBool
| Leq : BinOp TInt TInt TBool.

Inductive Expr Δ : BaseTp -> Type :=
| VarE: forall τ, Var Δ τ -> Expr Δ τ
| ConstA: Agent -> Expr Δ TAgent
| ConstR: Resource -> Expr Δ TResource
| ConstT: Time -> Expr Δ TTime
| ConstI: Z -> Expr Δ TInt
| ConstB: bool -> Expr Δ TBool
| BinOpE: forall {τ₁ τ₂ τ₃ : BaseTp} (b : BinOp τ₁ τ₂ τ₃)
                    (e₁ : Expr Δ τ₁) (e₂ : Expr Δ τ₂), Expr Δ τ₃.

Inductive Exprs (Δ : Env BaseTp) : Env BaseTp -> Type :=
| exprs_nil : Exprs Δ []
| exprs_cons {τ Δ'} (e : Expr Δ τ) (es : Exprs Δ Δ') : Exprs Δ (τ :: Δ').

Inductive Event :=
| Transfer : Agent -> Agent -> Resource -> Time -> Event.

Definition transmitExt (Δ : Env BaseTp) : Env BaseTp :=
  [TAgent; TAgent; TResource; TTime] ++ Δ.

Definition TemplateTp := Env BaseTp.

Inductive Contract (Γ : Env TemplateTp) (Δ : Env BaseTp) : Type :=
| Success: Contract Γ Δ
| Failure: Contract Γ Δ
| ContractApp: forall Δ' (f : Var Γ Δ') (vs : Exprs Δ Δ'),
    Contract Γ Δ
| Transmit : forall (p : Expr (transmitExt Δ) TBool)
                    (c : Contract Γ (transmitExt Δ)),
    Contract Γ Δ
| Alternative : forall (c₁ c₂ : Contract Γ Δ), Contract Γ Δ
| Parallel: forall (c₁ c₂ : Contract Γ Δ), Contract Γ Δ
| Sequential : forall (c₁ c₂ : Contract Γ Δ), Contract Γ Δ.
Notation " f • vs " := (ContractApp _ _ _ f vs) (at level 50, left associativity) : csl_scope.
Notation " c₁ + c₂ " := (Alternative _ _ c₁ c₂) (at level 50, left associativity) : csl_scope.
Notation " c₁ ∥ c₂ " := (Parallel _ _ c₁ c₂) (at level 50, left associativity) : csl_scope.
Notation " c₁ ; c₂ " := (Sequential _ _ c₁ c₂) (at level 50, left associativity) : csl_scope.
Open Scope csl_scope.

Global Instance ContractPOSet {Γ Δ} : POSet (Contract Γ Δ) := DiscretePOS (Contract Γ Δ).
Global Instance ContractωCPO {Γ Δ} : ωCPO (Contract Γ Δ) := DiscreteωCPO (Contract Γ Δ).

Definition TemplateDefs (Δ : Env BaseTp) (Γ : Env TemplateTp) : Type :=
  genProd Γ (fun Δ' => Contract Γ (Δ'++Δ)).

Definition GetContract {Δ Δ' Γ} (f : Var Γ Δ') : TemplateDefs Δ Γ -> Contract Γ (Δ' ++ Δ) :=
  genProdProj f.

Inductive TopLevel {Δ Γ}: TemplateDefs Δ Γ -> Contract Γ Δ -> Prop :=
  | LetRec: forall (D : TemplateDefs Δ Γ) (c : Contract Γ Δ), TopLevel D c.

Open Scope dom_scope.

Definition SemBaseTp (τ : BaseTp) :=
  match τ with
  | TAgent => Agent
  | TResource => Resource
  | TTime => Time
  | TInt => Z
  | TBool => bool
  end.
Notation "'τ[|' τ '|]'" := (SemBaseTp τ)(at level 9) : csl_scope.

Global Instance SemBaseTpPOSet {τ} : POSet (τ[|τ|]) := DiscretePOS τ[|τ|].
Global Instance SemBaseTpωCPO {τ : BaseTp} : ωCPO τ[|τ|] := DiscreteωCPO τ[|τ|].

Open Scope csl_scope.

Definition SemEnvBaseTp (Δ : Env BaseTp) := genProd Δ SemBaseTp.
Notation "'Δ[|' Δ '|]'" := (SemEnvBaseTp Δ)(at level 9) : csl_scope.

Lemma PreoEqSemEnvBaseTp{Δ} (δ δ' : Δ[|Δ|]) : δ ⊑ δ' -> δ = δ'.
Proof.
  induction Δ; intros; simpl in *; subst.
  - tauto.
  - destruct H; destruct δ; destruct δ'; simpl in *; rewrite H0; rewrite (IHΔ g g0);
    [reflexivity |  assumption].
Qed.

Lemma ωChainSemEnvBaseTp{Δ} (c : ωChain Δ[|Δ|]) : ⊔ c = c 0.
Proof.
  induction Δ; simpl in *; intros.
  + destruct c; rewrite mfun; reflexivity.
  + rewrite IHΔ; simpl; rewrite surjective_pairing; reflexivity.
Qed.

Fixpoint extendSemEnvBaseTp {Δ Δ'}:
  Δ[| Δ |] -> Δ[| Δ' |] -> Δ[|(Δ ++ Δ')|] :=
  match Δ with
  | nil => fun _ => fun δ' => δ'
  | _ :: _ => fun δ =>  fun δ' => (extendSemEnvBaseTp (fst δ) δ', snd δ)
  end.
Notation "δ₁ ⊕ δ₂" := (extendSemEnvBaseTp δ₁ δ₂) (at level 9) : csl_scope.

Definition SemVarBaseTp {Δ : Env BaseTp} {τ} (v: Var Δ τ) : Δ[|Δ|] ->ᶜ τ[|τ|] := genProdProjᶜ v.
Notation "'V[|' v '|]'":= (SemVarBaseTp v)(at level 9) : csl_scope.

Fixpoint EvalBinOp {τ₁ τ₂ τ₃} (b : BinOp τ₁ τ₂ τ₃) :
  τ[|τ₁|] -> τ[|τ₂|] -> τ[|τ₃|]:=
  match b with
  | AddZ => fun e1 => fun e2 => (e1 + e2)%Z
  | SubZ =>fun e1 => fun e2 => (e1 - e2)%Z
  | MulZ =>fun e1 => fun e2 => (e1 * e2)%Z
  | DivZ =>fun e1 => fun e2 => (e1 / e2)%Z
  | AndB =>fun e1 => fun e2 => andb e1 e2
  | OrB => fun e1 => fun e2 => orb e1 e2
  | Eq => fun e1 => fun e2 => Z.eqb e1 e2
  | Leq =>fun e1 => fun e2 => Z.leb e1 e2
  end.

Program Definition ContLiftBinOp {τ₁ τ₂ τ₃}
        (f : τ[|τ₁|] -> τ[|τ₂|] -> τ[|τ₃|]) : τ[|τ₁|] * τ[|τ₂|] ->ᶜ τ[|τ₃|] :=
    CF (MF (fun e => f (π₁ᶜ e) (π₂ᶜ e) ) _ ) _ .
  Next Obligation.
    intros ch₁ ch₂ [H₁ H₂]; rewrite H₁; rewrite H₂; reflexivity.
  Qed.
  Next Obligation.
    intros ch; simpl; reflexivity.
  Qed.
Notation "'B[|' b '|]'" := (ContLiftBinOp (EvalBinOp b)) (at level 9) : csl_scope.

(* Maps expressions to continuous functions *)
Fixpoint SemExpr {Δ τ} (e : Expr Δ τ): Δ[|Δ|] ->ᶜ τ[|τ|] :=
  match e with
  | VarE _ _ v => V[|v|]
  | ConstA _ a => constᶜ a
  | ConstR _ r => constᶜ r
  | ConstT _ t => constᶜ t
  | ConstI _ i => constᶜ i
  | ConstB _ b => constᶜ b
  | @BinOpE _ τ₁ τ₂ τ₃ b e₁ e₂ => B[|b|] ∘ᶜ ⟨SemExpr e₁, SemExpr e₂⟩ᶜ
  end.
Notation "'Q[|' e '|]^' d " := (SemExpr e d) (at level 9) : csl_scope.

Reserved Notation "'QS[|' es '|]^' d " (at level 9).
Fixpoint SemExprs {Δ Δ'} (es : Exprs Δ Δ') (δ : Δ[|Δ|]) : Δ[|Δ'|] :=
  match es with
  | exprs_nil _ => tt
  | exprs_cons _ e es' => (QS[|es'|]^δ, Q[|e|]^δ)
  end
where "'QS[|' es '|]^' d " := (SemExprs es d) : csl_scope.

(* Other notations for expressions used in the paper *)
Notation " δ '|=' p " := (Q[| p |]^δ = true) (at level 9) : csl_scope.

(* Denotations of Contracts *)

Definition SemEvent := (Agent * Agent * Resource * Time)%type.
Definition Trace := list SemEvent.

Program Instance PredTracePCPO: PCPO (Pred Trace) := mkPCPO (constᶜ False) _.
Next Obligation.
  contradiction.
Defined.

Definition SemTemplateTp (Δ : TemplateTp) := Δ[|Δ|] ->ᶜ Pred Trace.
Notation "'ΔC[|' Δ '|]'" := (SemTemplateTp Δ)(at level 9) : csl_scope.

Global Instance SemTemplateTpPOSet {Δ} : POSet ΔC[|Δ|].
Proof. apply CFunPOSet. Defined.
Global Instance SemTemplateTpωCPO {Δ} : ωCPO ΔC[|Δ|].
Proof. apply CFunωCPO. Defined.
Global Instance SemTemplateTpPCPO {Δ} : PCPO ΔC[|Δ|].
Proof. apply PtdFun. Defined.

Definition SemEnvTemplateTp (Γ : Env TemplateTp) : Type := genProd Γ SemTemplateTp.
Notation "'Γ[|' Γ '|]'" := (SemEnvTemplateTp Γ)(at level 9) : csl_scope.

Global Instance SemEnvTemplateTPPCPO {Γ} : PCPO Γ[|Γ|].
Proof.
  induction Γ; simpl; [exact PtdUnit |  exact PtdProd].
Defined.

Definition SemVarTemplateTp {Γ : Env TemplateTp} {Δ}
        (f: Var Γ Δ) : Γ[|Γ|] ->ᶜ ΔC[|Δ|] := genProdProjᶜ f.
Notation "'F[|' f '|]'":= (SemVarTemplateTp f)(at level 9) : csl_scope.

Lemma PreoγSemVarTemplateTp {Γ Δ'} (f : Var Γ Δ') (γ₁ γ₂ : Γ[|Γ|]):
  γ₁ ⊑ γ₂ -> F[|f|] γ₁ ⊑ F[|f|] γ₂.
  intros Preo δ' s H. induction Γ; simpl in *.
  + inversion f.
  + dependent destruction f.
    * destruct Preo. destruct γ₁, γ₂. simpl in *. apply H1. assumption.
    * destruct Preo. destruct γ₁, γ₂. simpl in *. apply IHΓ with (γ₁:=g); assumption.
Qed.

Lemma ωChainSemVarTemplateTp {Γ Δ'} (f : Var Γ Δ') (γω : ωChain Γ[| Γ|]) δ' s:
  F[| f|] (⊔ γω) δ' s -> exists n : nat, F[| f|] (γω n) δ' s.
  intros H; induction Γ; simpl in *.
  + inversion f.
  + dependent destruction f.
    * simpl in *. destruct H as [n H]. exists n; auto.
    * simpl in *.
      remember (π₁ᵐ ∘ᵐ γω : ωChain Γ[|Γ|]) as c.
      destruct (IHΓ f c) as [n IH];
      [subst c; exact H | ].
      exists n. subst; simpl in *. assumption.
Qed.

Definition transmitExtDom {Δ} (e : SemEvent) (δ : Δ[|Δ|]) : Δ[|transmitExt Δ|] :=
  match e with
    | (a₁, a₂, r, t) => (δ, t, r, a₁, a₁)
  end.

Program Definition condCons {Δ} (b : Expr (transmitExt Δ) TBool) (δ : Δ[|Δ|])
        (denot : Δ[| transmitExt Δ|] -> Pred Trace): Pred Trace :=
  CF (MF (fun s => match s with
                   | [] => False
                   | e :: s' =>
                     let δ' := transmitExtDom e δ in
                     denot δ' s' /\ Q[|b|]^δ' = true
                   end) _ ) _.
Next Obligation.
  intros ch w; simpl; exists 0; tauto.
Qed.

Reserved Notation "'C[|' c '|]^(' g '|' d ')'" (at level 9).
Fixpoint SemContract {Γ Δ} (c : Contract Γ Δ) (δ : Δ[|Δ|]) (γ : Γ[|Γ|]) : Pred Trace :=
  match c with
  | Success _ _ => singleton []
  | Failure _ _ => ⊥
  | f • es => F[|f|] γ (QS[|es|]^δ)
  | Transmit _ _ p c' => condCons p δ (fun δ₀ => SemContract c' δ₀ γ)
  | c₁ + c₂ => sum C[|c₁|]^(γ|δ) C[|c₂|]^(γ|δ)
  | c₁ ∥ c₂ => interleave C[|c₁|]^(γ|δ) C[|c₂|]^(γ|δ)
  | c₁ ; c₂ => Pred.concat C[|c₁|]^(γ|δ) C[|c₂|]^(γ|δ)
  end
where "'C[|' c '|]^(' g '|' d ')'" := (SemContract c d g) : csl_scope.

Lemma PreoγSemContract {Γ Δ}(c : Contract Γ Δ)
      (δ : Δ[|Δ|])(γ₁ γ₂ : Γ[|Γ|]):
  γ₁ ⊑ γ₂ -> forall s, C[|c|]^(γ₁ | δ) s -> C[|c|]^(γ₂ | δ) s.
Proof.
  induction c; intros Preo s H; simpl in *; auto.
  + apply (PreoγSemVarTemplateTp f) in Preo. apply Preo. assumption.
  + destruct s.
    * contradiction.
    * destruct H. split; [ | assumption].
    specialize IHc with (transmitExtDom s δ) s0. apply IHc; auto.
  + destruct H; [left | right]; auto.
  + destruct H as [[l1 l2] [H0 [H1 H2]]]. simpl in *.
    exists (l1, l2); repeat split; auto.
  + destruct H as [[l1 l2] [H0 [H1 H2]]]. simpl in *.
    exists (l1, l2); repeat split; auto.
Qed.

Instance monotone_SemContract {Γ Δ} (c : Contract Γ Δ) :
  Proper (preo ==> preo ==> preo) (SemContract c).
Proof.
  intros δ₁ δ₂ Preoδ γ₁ γ₂ Preoγ.
  simpl; intros s H. apply PreoEqSemEnvBaseTp in Preoδ. subst.
  apply PreoγSemContract with (γ₁0:= γ₁); assumption.
Defined.

Lemma ωChainγSemContract {Γ Δ}(c : Contract Γ Δ)
      (δ : Δ[|Δ|])(γω : ωChain Γ[| Γ|]):
  forall s, C[| c |]^( ⊔ γω | δ) s ->
  exists n : nat, C[| c |]^( γω n | δ) s.
  induction c; intros; simpl in *.
  + subst; eexists; eauto. exact 0.
  + contradiction.
  + apply ωChainSemVarTemplateTp; assumption.
  + destruct s.
    * contradiction.
    * destruct H. destruct (IHc (transmitExtDom s δ) s0) as [n IH]; auto.
      exists n; split; auto.
  + destruct H; [destruct (IHc1 δ s) as [n IH] | destruct (IHc2 δ s) as [n IH]]; auto; exists n; [left | right]; auto.
  + destruct H as [[l1 l2] [H0 [H1 H2]]]. simpl in *.
    destruct (IHc1 δ l1) as [n1 IH1]; auto. destruct (IHc2 δ l2) as [n2 IH2]; auto.
    exists (n1+n2)%nat.
    exists (l1, l2); repeat split; auto; simpl.
    eapply monotone_SemContract in IH1; eauto. reflexivity.
    apply monotone; simpl. auto with arith.
    eapply monotone_SemContract in IH2; eauto. reflexivity.
    apply monotone; simpl. auto with arith.
  + destruct H as [[l1 l2] [H0 [H1 H2]]]. simpl in *.
    destruct (IHc1 δ l1) as [n1 IH1]; auto. destruct (IHc2 δ l2) as [n2 IH2]; auto.
    exists (n1+n2)%nat.
    exists (l1, l2); repeat split; auto; simpl.
    eapply monotone_SemContract in IH1; eauto. reflexivity.
    apply monotone; simpl. auto with arith.
    eapply monotone_SemContract in IH2; eauto. reflexivity.
    apply monotone; simpl. auto with arith.
Qed.

Program Definition stepSemContract {Γ Δ Δ'}
        (δ : Δ[|Δ|]) (c : Contract Γ (Δ'++Δ)) : Γ[|Γ|] ->ᶜ ΔC[|Δ'|] :=
  CF (MF (fun γ => CF (MF (fun δ' => C[|c|]^(γ |δ' ⊕ δ )) _ ) _ ) _ ) _ .
Next Obligation.
  intros δ₁ δ₂ Ord s H. apply PreoEqSemEnvBaseTp in Ord; subst. assumption.
Qed.
Next Obligation.
  intros ch s; simpl; intro H; exists 0.
  rewrite <- ωChainSemEnvBaseTp; assumption.
Qed.
Next Obligation.
  intros γ₁ γ₂ Ord δ' s H. simpl in *. eapply monotone_SemContract with (c0:=c) in Ord; eauto. eapply Ord. eassumption. reflexivity.
Qed.
Next Obligation.
  intros ch δ' s H; simpl in *. apply ωChainγSemContract. assumption.
Qed.

Program Definition stepTemplateDefs {Γ Δ}
        (δ:Δ[|Δ|]) (D: TemplateDefs Δ Γ) :=
  genProdTrans (fun Δ' => fun c => stepSemContract δ c) D.

Program Definition γUpdate {Γ Δ} (D : TemplateDefs Δ Γ) (δ : Δ[|Δ|]) : Γ[|Γ|] ->ᶜ Γ[|Γ|] :=
  genMedᶜ (stepTemplateDefs δ D).

Definition TemplateDefsDom {Γ Δ} (D : TemplateDefs Δ Γ) (δ : Δ[|Δ|]) : Γ[|Γ|] :=
  fixp (γUpdate D δ).
Notation "'D[|' D '|]^' d" := (TemplateDefsDom D d)(at level 9) : csl_scope.

Lemma SemTemplateDefs_unfold {Γ Δ} {D : TemplateDefs Δ Γ} {δ : Δ[|Δ|]} :
  forall Δ' (f : Var Γ Δ'),
    let γ := D[|D|]^δ in
    F[| f |] γ ≡ stepSemContract δ (GetContract f D) γ.
Proof.
  intros; subst γ; unfold TemplateDefsDom, GetContract.
  transitivity (F[|f|] ((γUpdate D δ) (fixp (γUpdate D δ))));
    [ rewrite <- fixp_unfold; reflexivity | ].
  unfold γUpdate.
  remember (fixp (genMedᶜ (stepTemplateDefs δ D))) as γ; simpl in γ.
  unfold stepTemplateDefs.
  remember (fun (Δ':TemplateTp) => fun c => stepSemContract (Γ:=Γ) (Δ':=Δ') δ c) as F.
  rewrite (genProdProjᶜ_genMed (F:=F) f D γ).
  subst F; reflexivity.
Qed.

Definition TopLevelDom {Δ Γ c} {D : TemplateDefs Δ Γ} (E : TopLevel D c) (δ : Δ[|Δ|]) : Pred Trace :=
  let γ := D[|D|]^δ in C[| c |]^(γ|δ).
Notation "'E[|' E '|]^' d" := (TopLevelDom E d)(at level 9) : csl_scope.

(* Other notations used in the paper *)

Notation " ld '|=^(' D '|' d ')' s '=>' c" := (C[|c|]^(  D[| D |]^d | ld⊕ d) s)(at level 9) : csl_scope.

Definition ContractEq {Δ Γ Δ'}
           (D : TemplateDefs Δ Γ)
           (δ : Δ[|Δ|])
           (c : Contract Γ (Δ' ++ Δ))
           (c': Contract Γ (Δ' ++ Δ)) :=
  let γ := D[|D|]^δ in forall (δ' : Δ[|Δ'|]),
      C[|c|]^(γ | δ' ⊕ δ) ≡  C[|c'|]^(γ | δ' ⊕ δ).

Definition ContractSubset {Δ Γ Δ'}
           (D : TemplateDefs Δ Γ)
           (δ : Δ[|Δ|])
           (c : Contract Γ (Δ' ++ Δ))
           (c': Contract Γ (Δ' ++ Δ)) :=
  let γ := D[|D|]^δ in forall δ',
      C[|c|]^(γ | δ' ⊕ δ)  ⊑ C[|c'|]^(γ | δ' ⊕ δ).

(* Contract Satisfaction *)

Reserved Notation " ld '|-^(' D '|' d ')' s '=>' c" (at level 9).

Inductive ContractSat {Δ Δ' Γ}:
  TemplateDefs Δ Γ -> Δ[|Δ|] -> Contract Γ (Δ' ++ Δ) -> Δ[|Δ'|] -> Trace -> Prop :=
| cs_succ: forall δ' D δ,
    δ' |-^(D | δ) [] => (Success _ _)
| cs_contr_app:
    forall δ (D : TemplateDefs Δ Γ) δ' s Δ'' (f : Var Γ Δ'') (es : Exprs (Δ'++Δ) Δ''),
      ContractSat (Δ':=Δ'') D δ (GetContract f D) (QS[|es|]^ (δ' ⊕ δ)) s ->
      δ' |-^(D|δ) s => (f • es)
| cs_transmit:
    forall δ' D δ s e
           (p : Expr (transmitExt Δ' ++ Δ) TBool)
           (c : Contract Γ (transmitExt (Δ' ++ Δ)))
           δ'',
      δ'' = transmitExtDom e δ' ->
      (transmitExtDom e (δ' ⊕ δ)) |=  p ->
      ContractSat (Δ':=transmitExt Δ') D δ c δ'' s ->
      δ' |-^(D|δ) (e :: s) => (Transmit _ _ p c)
| cs_alt1: forall δ' D δ s c1 c2,
    δ' |-^(D|δ) s => c1 ->
    δ' |-^(D|δ) s => (c1 + c2)
| cs_alt2: forall δ' D δ s c1 c2,
    δ' |-^(D|δ) s => c2 ->
    δ' |-^(D|δ) s => (c1 + c2)
| cs_par: forall δ' D δ s1 s2 c1 c2,
    δ' |-^(D|δ) s1 => c1 ->
    δ' |-^(D|δ) s2 => c2 ->
    forall s, merge s1 s2 s ->
    δ' |-^(D|δ) s  => (c1 ∥ c2)
| cs_seq: forall δ' D δ s1 s2 c1 c2,
    δ' |-^(D|δ) s1 => c1 ->
    δ' |-^(D|δ) s2 => c2 ->
    δ' |-^(D|δ) (s1 ++ s2) => (c1 ; c2)
where " ld '|-^(' D '|' d ')' s '=>' c" := (ContractSat D d c ld s) : csl_scope.
Hint Constructors ContractSat : csl.

Global Instance ContractSatPOSet {Δ Δ' Γ D δ c δ' s} : POSet (ContractSat D δ c δ' s) := DiscretePOS (@ContractSat Δ Δ' Γ D δ c δ' s).
Global Instance ContractSatωCPO {Δ Δ' Γ D δ c δ' s} : ωCPO (ContractSat D δ c δ' s) := DiscreteωCPO (@ContractSat Δ Δ' Γ D δ c δ' s).

Global Instance TracePOSet : POSet Trace := DiscretePOS Trace.

Global Instance monotone_ContractSat {Δ Δ' Γ D δ} : Proper (preo ==> preo ==> preo ==> preo) (@ContractSat Δ Δ' Γ D δ).
Proof.
  intros c1 c2 LEc d1 d2 LEd s1 s2 LEs H; simpl in *; apply PreoEqSemEnvBaseTp in LEd; subst. assumption.
Defined.

Lemma SemEnvExtend_comm:
  forall Δ' Δ e (δ: Δ[|Δ|]) (δ' : Δ[|Δ'|]),
    transmitExtDom e δ' ⊕ δ = (transmitExtDom e δ') ⊕ δ.
  intros; destruct e as [[[a₁ a₂] r] t]; simpl; reflexivity.
Qed.

Lemma soundC Δ' Γ Δ (δ': Δ[|Δ'|])(D : TemplateDefs Δ Γ) (δ : Δ[|Δ|])
      s (c : Contract Γ _ ):
  δ' |-^(D|δ) s => c ⊑
  C[|c|]^( D[|D|]^δ | δ' ⊕ δ) s.
  intro HSat; induction HSat; simpl; subst; try (exists (s1, s2)); eauto.
  + apply SemTemplateDefs_unfold; assumption.
  + split; [rewrite SemEnvExtend_comm | ]; assumption.
Qed.

Program Definition stepContractSat {Γ Δ} (D : TemplateDefs Δ Γ) (δ : Δ[|Δ|]) Δ':=
  CF (MF (fun (c : Contract Γ (Δ'++Δ)) =>
            CF ( MF (fun (δ':Δ[|Δ'|]) =>
                       CF ( MF (fun (s : Trace) => ContractSat D δ c δ' s ) _ ) _  ) _ ) _ ) _ ) _.
Next Obligation.
  intros s1 s2 EQs H; subst; assumption.
Qed.
Next Obligation.
  intros ch s. simpl in *. exists 0; assumption.
Qed.
Next Obligation.
  intros δ₁ δ₂ LEδ s H; simpl in *. apply PreoEqSemEnvBaseTp in LEδ; subst; assumption.
Qed.
Next Obligation.
  intros ch s H; simpl in *. rewrite ωChainSemEnvBaseTp in H; exists 0; assumption.
Qed.
Next Obligation.
  intros c1 c2 Eqc δ' s H; simpl in *; subst; assumption.
Qed.
Next Obligation.
  intros ch δ' s H; simpl in *. exists 0; assumption.
Qed.

Program Definition γ'_def {Γ Δ} (D : TemplateDefs Δ Γ) (δ : Δ[|Δ|]) : Γ[|Γ|] :=
  genProdTransᶜ (stepContractSat D δ) D.

Lemma γ'_unfold {Δ Γ} (D : TemplateDefs Δ Γ) (δ : Δ[|Δ|]):
  forall Δ' (f : Var Γ Δ'),
    let γ' := γ'_def D δ in
     F[|f|] γ' = stepContractSat D δ Δ' (GetContract f D).
  intros; subst γ'. unfold GetContract, SemVarTemplateTp, γ'_def.
  remember (fun (Δ':TemplateTp) => stepContractSat D δ Δ') as F.
  exact (genProdProjᶜ_genProdTrans (F:=F) f D).
Qed.

Lemma completeC_helper Γ Δ Δ' (D : TemplateDefs Δ Γ) δ c:
  let γ' := γ'_def D δ in
  forall (δ' : Δ[|Δ'|]) s,
    C[|c|]^(γ' | (δ'⊕δ)) s ≡ δ' |-^(D|δ) s => c.
  generalize dependent δ; generalize dependent D;
  dependent induction c; intros; simpl; eauto; autounfold in *.
  + split; intro; try inversion H; subst; auto with csl.
  + split; intro; [contradiction | inversion H].
  + rewrite γ'_unfold; split; intro; auto with csl.
    inversion H; simpl_existTs; subst; assumption.
  + destruct s; split; intro; simpl in *;
      [contradiction | inversion H | destruct H| inversion H; subst; split]; auto.
    * econstructor; eauto; eapply IHc; try rewrite <- SemEnvExtend_comm; auto.
    * apply IHc in H9; try rewrite SemEnvExtend_comm; auto.
  + split; intro H; [destruct H | ].
    * apply cs_alt1; apply IHc1; auto.
    * apply cs_alt2; apply IHc2; auto.
    * inversion H; subst; [left; apply IHc1 | right; apply IHc2 ]; auto.
  + split; intro H.
    * destruct H as [[s1 s2] [HM [Hs1 Hs2]]]; simpl in *;
        econstructor; eauto; [apply IHc1 | apply IHc2]; auto.
    * inversion H; subst; exists (s1, s2); repeat split; [ | apply IHc1 |apply IHc2]; auto.
  + split; intro H.
    * destruct H as [[s1 s2] [HM [Hs1 Hs2]]]; simpl in *; subst.
      econstructor; eauto; [apply IHc1 | apply IHc2]; auto.
    * inversion H; subst. exists (s1, s2); repeat split; [ apply IHc1 | apply IHc2]; auto.
Qed.

Lemma γ'_unfolds_like_γ {Γ Δ} (D : TemplateDefs Δ Γ) (δ:Δ[|Δ|]):
   forall Δ' (f : Var Γ Δ'),
    let γ' := γ'_def D δ in
     F[|f|] γ' ≡ stepSemContract δ (GetContract f D) γ'.
  intros. subst γ'. rewrite γ'_unfold. apply eqextᵐ. intro δ'. apply eqextᵐ. intro s. simpl.
  symmetry; apply completeC_helper.
Qed.

(* Trivial observation: D[|D|]^δ is a fixpoint of γUpdate *)
Lemma γUpdate_TemplateDefsDom {Γ Δ} (D : TemplateDefs Δ Γ) (δ:Δ[|Δ|]):
  γUpdate D δ D[|D|]^δ  ≡ D[|D|]^δ.
  unfold TemplateDefsDom.
  rewrite <- fixp_unfold.
  reflexivity.
Qed.

(* γ'_def is also a fixpoint of γUpdate *)
Lemma γUpdate_γ'_def {Γ Δ} (D : TemplateDefs Δ Γ) (δ:Δ[|Δ|]):
  γUpdate D δ (γ'_def D δ)  ≡ γ'_def D δ.
  apply genProdEqᶜ.
  unfold γUpdate, stepTemplateDefs; intros.
  remember (fun (Δ' : TemplateTp) (c : Contract Γ (Δ' ++ Δ)) => stepSemContract δ c) as F.
  rewrite (genProdProjᶜ_genMed (F:=F) v D (γ'_def D δ)). subst F; symmetry.
  fold (GetContract v D); rewrite (γ'_unfolds_like_γ D δ a v).
  reflexivity.
Qed.

Lemma γPreoγ' {Γ Δ} (D : TemplateDefs Δ Γ) (δ:Δ[|Δ|]):
  let γ := D[|D|]^δ in
  let γ' := γ'_def D δ in
  γ ⊑ γ'.
Proof.
  intros; subst γ γ'.
  unfold TemplateDefsDom.
  apply fixp_smaller.
  exact (γUpdate_γ'_def D δ).
Qed.

Lemma completeC Δ' Γ Δ (δ': Δ[|Δ'|])
      (D : TemplateDefs Δ Γ) (δ : Δ[|Δ|])
      s (c : Contract Γ (Δ' ++ Δ)):
  δ' |=^(D|δ) s => c ⊑ δ' |-^(D|δ) s => c.
  rewrite <- completeC_helper. rewrite γPreoγ'. reflexivity.
Qed.

(* Denotational characterization of contract satisfaction *)
Theorem Theorem1:
  forall  Δ' Γ Δ (δ': Δ[|Δ'|])
      (D : TemplateDefs Δ Γ) (δ : Δ[|Δ|])
      (s : Trace) (c : Contract Γ _ ),
    C[|c|]^( D[|D|]^δ | δ' ⊕ δ) s ≡ δ' |-^(D|δ) s => c.
Proof.
  split; [apply completeC | apply soundC].
Qed.
