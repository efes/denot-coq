Require Import Domains.
Require Import Pred.
Require Import List.
Require Import PeanoNat.

Arguments impl _ _ /.

Parameter Σ : Set.
Parameter cmpΣ : forall a b : Σ, {a = b} + {a <> b}.

Inductive RE : Set :=
| ε : RE
| lit : Σ -> RE
| cat : RE -> RE -> RE
| amb : RE -> RE -> RE
| star : RE -> RE.

Notation "# a" := (lit a) (at level 5).
Notation "e₁ · e₂" := (cat e₁ e₂) (at level 40, left associativity).
Notation "e₁ ⊕ e₂" := (amb e₁ e₂) (at level 50, left associativity).
(*Notation "e *" := (star e) (at level 60).*)

Definition word := list Σ.
Definition D := Pred word.

Reserved Notation "〚 e 〛" (at level 0).

Fixpoint den (e : RE) : D :=
  match e with
  | ε       => singleton []
  | # a     => singleton [a]
  | e₁ · e₂ => Pred.concat 〚e₁〛 〚e₂〛
  | e₁ ⊕ e₂ => sum 〚e₁〛 〚e₂〛
  | star e  => fixp (sum (singleton []) ∘ᶜ Pred.concat 〚e〛)%dom
  end
where "〚 e 〛" := (den e).

Inductive matches : RE -> word -> Prop :=
| m_ε :
    matches ε []
| m_lit a :
    matches (# a) (a :: [])
| m_cat {e₁ e₂ w₁ w₂}
         (H₁ : matches e₁ w₁)
         (H₂ : matches e₂ w₂) :
    matches (e₁ · e₂) (w₁ ++ w₂)
| m_alt₁ {e₁} e₂ {w}
         (H : matches e₁ w) :
    matches (e₁ ⊕ e₂) w
| m_alt₂ e₁ {e₂ w}
         (H : matches e₂ w) :
    matches (e₁ ⊕ e₂) w
| m_star {e w}
         (H : matches (ε ⊕ e · star e) w) :
    matches (star e) w.

Inductive wstar (P : word -> Prop) : word -> Prop :=
| ws_emp : wstar P []
| ws_cat w₁ w₂ (HP : P w₁) (HPs : P w₂) : wstar P (w₁ ++ w₂).

Inductive matches_comp : RE -> word -> Prop :=
| mc_ε :
    matches_comp ε []
| mc_lift a :
    matches_comp (#a) (a :: [])
| mc_cat {e₁ e₂ w₁ w₂}
         (H₁ : matches_comp e₁ w₁)
         (H₂ : matches_comp e₂ w₂) :
    matches_comp (e₁ · e₂) (w₁ ++ w₂)
| mc_alt₁ {e₁} e₂ {w}
         (H : matches_comp e₁ w) :
    matches_comp (e₁ ⊕ e₂) w
| mc_alt₂ e₁ {e₂ w}
         (H : matches_comp e₂ w) :
    matches_comp (e₁ ⊕ e₂) w
| mc_star {e w}
          (H : wstar (matches_comp e) w) :
    matches_comp (star e) w.

Lemma sound e w (HM : matches e w) : den e w.
Proof.
  induction HM; try (simpl; now eauto); [|].
  - exists (w₁, w₂); simpl; now eauto.
  - change (fixp (sum (singleton []) ∘ᶜ Pred.concat 〚e〛)%dom w); rewrite fixp_unfold; apply IHHM.
Qed.

Lemma complete e w (HD : den e w) : matches e w.
Proof.
  revert w HD; induction e; intros; try (inversion HD; subst; now eauto using matches).
  - destruct HD as [[ys zs] [EQ [Hy Hz]]]; subst; now eauto using matches.
  - change (fixp (sum (singleton []) ∘ᶜ Pred.concat 〚e〛)%dom w) in HD; revert w HD; apply fixp_ind.
    + split; intros c HM w [n HD]; simpl in HD; now eauto.
    + intros w [].
    + intros d HH w HI; apply m_star.
      destruct HI as [HI | [[ys zs] [EQ [Hy Hz]]]]; [inversion HI |]; simpl in *; subst; auto using matches.
Qed.

