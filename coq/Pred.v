Require Import Domains.
Require Import List.
Require Import PeanoNat.
Arguments impl _ _ /.

Section Pred.
  Context (A : Type).
  Local Instance APOS : POSet A := DiscretePOS A.
  Local Instance ACPO : ωCPO  A := DiscreteωCPO A.
  Definition Pred := A ->ᶜ Prop.
End Pred.

Section PredProp.
  Local Open Scope dom_scope.
  Program Definition empty {A} : Pred A := ⊥.

  Program Definition singleton {A} (a : A) : Pred A :=
    CF (MF (fun b => a = b) _) _.
  Next Obligation.
    intros c Hc; simpl in *; exists 0; assumption.
  Qed.

  Program Definition liftRel {A B} (P : A -> B -> Prop) : Pred A ->ᶜ Pred B :=
    CF (MF (fun f => CF (MF (fun b => exists a, P a b /\ f a) _) _) _) _.
  Next Obligation.
    intros b₁ b₂ EQb HC; inversion EQb; subst; assumption.
  Qed.
  Next Obligation.
    intros c Hc; exists 0; assumption.
  Qed.
  Next Obligation.
    intros f₁ f₂ LEf b [a [HP H1]]; eexists; split; [| apply LEf]; eassumption.
  Qed.
  Next Obligation.
    intros c b [a [HP [n Hc]]]; simpl in *; eauto.
  Qed.

  Program Definition cart {A B} : Pred A * Pred B ->ᶜ Pred (A * B) :=
    CF (MF (fun pf => CF (MF (fun pa => fst pf (fst pa) /\ snd pf (snd pa)) _) _) _) _.
  Next Obligation.
    intros b₁ b₂ EQb HC; inversion EQb; subst; assumption.
  Qed.
  Next Obligation.
    intros c Hc; exists 0; assumption.
  Qed.
  Next Obligation.
    intros [f₁ g₁] [f₂ g₂] [LEf LEg] [a b]; simpl in *; now intuition.
  Qed.
  Next Obligation.
    intros c [a b] [[m Ha] [n Hb]]; exists (max m n); simpl in *; split.
    - assert (HH : c m ⊑ c (max m n)) by apply monotone, Nat.le_max_l.
      apply HH, Ha.
    - assert (HH : c n ⊑ c (max m n)) by apply monotone, Nat.le_max_r.
      apply HH, Hb.
  Qed.

  Definition concat {A} : Pred (list A) ->ᶜ Pred (list A) ->ᶜ Pred (list A) :=
    curryᶜ (liftRel (fun xy z => fst xy ++ snd xy = z) ∘ᶜ cart)%dom.

  Program Definition sum {A} : Pred (list A) ->ᶜ Pred (list A) ->ᶜ Pred (list A) :=
    curryᶜ (postcompᶜ (CF (MF (fun p => fst p \/ snd p) _) _) ∘ᶜ (CF (MF (fun p => ⟨ fst p, snd p ⟩ᶜ) _) _))%dom.
  Next Obligation.
    intros [p q] [r s]; simpl; tauto.
  Qed.
  Next Obligation.
    intros c [[n H] | [n H]]; simpl in *; now eauto.
  Qed.
  Next Obligation.
    intros [p₁ q₁] [p₂ q₂] [LEp LEq] w; simpl in *; auto.
  Qed.
  Next Obligation.
    intros c w; simpl; tauto.
  Qed.

  Fixpoint merge{A} (l₁ : list A) (l₂ : list A) (res : list A) : Prop :=
    match l₁, l₂, res with
      | [], [], [] => True
      | h₁ :: l₁', h₂ :: l₂', hᵣ :: res' =>
        (h₁ = hᵣ /\ merge l₁' l₂ res') \/ (h₂ = hᵣ /\ merge l₁ l₂' res')
      | [], l₂, res => l₂ = res
      | l₁, [], res => l₁ = res
      | _, _, _ => False
    end.

  Definition interleave {A} : Pred (list A) ->ᶜ Pred (list A) ->ᶜ Pred (list A) :=
    curryᶜ (liftRel (fun xy z => merge (fst xy) (snd xy) z) ∘ᶜ cart)%dom.

  Program Definition tail {A} (f : Pred (list A)) (a : A) : Pred (list A) :=
    CF (MF (fun s => f (a :: s)) _ ) _.
  Next Obligation.
    intros ch g; simpl; exists 0; auto.
  Qed.
End PredProp.

Lemma merge_nil_R {A}: forall (s : list A), merge s [] s.
  induction s; simpl; auto.
Qed.
Lemma merge_nil_L {A}: forall (s : list A), merge [] s s.
  induction s; simpl; auto.
Qed.
Lemma merge_nil_REq {A} : forall s s' : list A, merge s [] s' -> s = s'.
  induction s; destruct s'; intros; simpl; auto.
Qed.
Lemma merge_nil_LEq {A} : forall s s' : list A, merge [] s s' -> s = s'.
  induction s; destruct s'; intros; simpl; auto.
Qed.
Lemma merge_cons_L {A} : forall t₁ t₂ s : list A, forall e,
    merge t₁ t₂ s -> merge (e::t₁) t₂ (e::s).
  intros; simpl; destruct t₂; auto; apply merge_nil_REq in H; subst; auto.
Qed.
Lemma merge_cons_R {A} : forall t₁ t₂ s : list A, forall e,
    merge t₁ t₂ s -> merge t₁ (e::t₂) (e::s).
  intros; simpl; destruct t₁; auto. apply merge_nil_LEq in H; subst; auto.
Qed.
Hint Resolve merge_nil_R : pred.
Hint Resolve merge_nil_L : pred.
Hint Resolve merge_cons_R : pred.
Hint Resolve merge_cons_L : pred.
Hint Resolve merge_nil_REq : pred.
Hint Resolve merge_nil_LEq : pred.
