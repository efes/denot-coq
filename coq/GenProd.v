Require Import Domains.
Require Import Morphisms.
Require Import List.

(* Generalized product *)

Inductive Var {Tp} : list Tp -> Tp -> Type :=
| V0 : forall Γ τ, Var (τ :: Γ) τ
| VS : forall Γ τ τ', Var Γ τ -> Var (τ' :: Γ) τ.

Fixpoint genProd {A} (L : list A) (f : A -> Type) : Type :=
  match L with
  | nil => ()
  | a :: L' => (genProd L' f) * (f a)
  end%type.

Definition med {A B C} (f : C -> A) (g: C -> B) : C -> A * B :=
  fun x => (f x, g x).

Definition tens {A B C D} (f : C -> A) (g: D -> B) : C * D -> A * B :=
  fun x => (f (fst x), g (snd x)).

Definition uncurry {A B C} (f : A -> B -> C) : A * B -> C :=
  fun xy => f (fst xy) (snd xy).

Fixpoint genProdTrans {A} {f g : A -> Type} {L} (F : forall x, f x  -> g x) :
  genProd L f -> genProd L g :=
  match L with
  | nil => id
  | a :: L' => tens (genProdTrans F) (F a)
  end%type.

Fixpoint genMed {A B L} {f : A -> Type} :
  genProd L (fun x => B -> f x) -> (B -> genProd L f) :=
  match L with
  | nil => fun _ => const ()
  | a :: L' => (uncurry med) ∘ (med (genMed ∘ fst) snd)
  end.

Fixpoint genProdProj {A L a} {f : A -> Type} (v : Var L a) : genProd L f -> f a :=
  match v with
  | V0 _ _ => snd
  | VS _ _ _ v' => (genProdProj v') ∘ fst
  end.

Lemma genMed₀{A B} {f : A -> Type} (Π : genProd nil (fun x => B -> f x)) :
  genMed Π = const ().
Proof.
  destruct Π. simpl. reflexivity.
Qed.

Lemma genMed₁{A B a L} {f : A -> Type} (Π : genProd (a :: L) (fun x => B -> f x)) :
  fst ∘ (genMed Π) = genMed (fst Π).
Proof.
  apply functional_extensionality; intros; simpl; reflexivity.
Qed.

Lemma genMed₂ {A B a L} {f : A -> Type} (Π : genProd (a :: L) (fun x => B -> f x)) :
  snd ∘ (genMed Π) = snd Π.
Proof.
  apply functional_extensionality; intros; simpl; reflexivity.
Qed.

Lemma genProdEq {A L}{f : A -> Type} (P Q : genProd L f) :
  (forall a, forall (v : Var L a), genProdProj v P = genProdProj v Q) -> P = Q.
  induction L; simpl in *; intros; destruct P; destruct Q.
  + reflexivity.
  + rewrite IHL with g g0.
    * specialize H with a (V0 _ _ ); simpl in H; subst; reflexivity.
    * intros. specialize H with a0 (VS _ _ _ v) . simpl in H. auto.
Qed.

Lemma genProdProj_genMed {A B L a} {f  g : A -> Type} {F : forall x, g x  -> (B -> f x)}
      (v : Var L a) (P : genProd L g):
  (genProdProj v) ∘ (genMed (genProdTrans F P)) = F a (genProdProj v P).
Proof.
  induction L; simpl in *; dependent destruction v.
  + reflexivity.
  + destruct P; simpl.
    unfold uncurry, med, tens, compose in *; simpl in *.
    apply IHL.
Qed.

Lemma genProdProj_genProdTrans {A L a} {f  g : A -> Type} {F : forall x, g x  -> f x}
      (v : Var L a) (P : genProd L g):
  genProdProj v (genProdTrans F P) = F a (genProdProj v P).
Proof.
  induction L; simpl in *; dependent destruction v.
  + reflexivity.
  + destruct P; simpl.
    unfold uncurry, med, tens, compose in *; simpl in *.
    apply IHL.
Qed.

Section GenProdPOS.
  Context {A} {f : A -> Type} `{forall a, POSet (f a)}.

  Global Instance POSgenProd (L : list A) : POSet (genProd L f).
  Proof.
    induction L; simpl; [exact POSunit | exact POSprod].
  Defined.
End GenProdPOS.

Definition U {A B} `{POSet A} `{POSet B} (f : A ->ᵐ B) : A -> B := mfun f.

Open Scope dom_scope.
Section GenProdDefs₁.
  Context {A B} {f : A -> Type} {g : A -> Type}
          `{forall a, POSet (f a)} `{forall a, POSet (g a)} `{POSet B}.

  Program Definition genMedᵐ {L} (Π : genProd L (fun x => B ->ᵐ f x)) : B ->ᵐ genProd L f :=
    MF (genMed (genProdTrans (fun x => U) Π)) _ .
  Next Obligation.
    intros b₁ b₂ LEb.
    induction L; simpl in *.
    + reflexivity.
    + split; destruct Π; [apply IHL | simpl].
      unfold U. apply monotone. assumption.
  Qed.

  Program Definition genProdTransᵐ {L}
          (F : forall x, f x  ->ᵐ g x) : genProd L f ->ᵐ genProd L g :=
    MF (genProdTrans (fun x => U (F x))) _.
  Next Obligation.
    intros b₁ b₂ LEb. induction L; simpl in *.
    + subst; reflexivity.
    +split; destruct b₁, b₂, LEb; simpl in *;
       [apply IHL | unfold U; apply monotone]; assumption.
  Qed.

  Program Definition genProdProjᵐ {L a} (v : Var L a) : genProd L f ->ᵐ f a :=
    MF (genProdProj v) _.
  Next Obligation.
    intros P₁ P₂ LEP. induction v; destruct P₁, P₂; destruct LEP; simpl in *.
    + assumption.
    + specialize IHv with g0 g1. apply IHv in H2. rewrite H2. reflexivity.
  Qed.

  Global Instance monotone_genMedᵐ (L : list A) F: Proper (preo ++> preo) (genMedᵐ (L:=L) F).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.
  Global Instance equiv_genMedᵐ (L : list A) F: Proper (equiv ++> equiv) (genMedᵐ (L:=L) F).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.
  Global Instance monotone_genProdTransᵐ (L : list A) F :
    Proper (preo ++> preo) (genProdTransᵐ (L:=L) F).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.
  Global Instance equiv_genProdTransᵐ (L : list A) F :
    Proper (equiv ++> equiv) (genProdTransᵐ (L:=L) F).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.
End GenProdDefs₁.

Section GenProdPropsPOS.
  Context {A B}  {f g h: A -> Type}
          `{forall a, POSet (f a)} `{forall a, POSet (h a)}
          `{poB : POSet B}.

  (** genProd L f is a generalized product *)
  Lemma genMedᵐ₀ (Π : genProd nil (fun x => B ->ᵐ f x)) :
     genMedᵐ Π ≡ constᵐ ().
  Proof.
    apply eqextᵐ; intros; simpl; reflexivity.
  Qed.

  Lemma genMedᵐ₁ {a L} (Π : genProd (a :: L) (fun x => B ->ᵐ f x)) :
    π₁ᵐ ∘ᵐ (genMedᵐ Π) ≡ genMedᵐ (π₁ᵐ Π).
  Proof.
    apply eqextᵐ; intros; simpl; reflexivity.
  Qed.

  Lemma genMedᵐ₂ {a L} (Π : genProd (a :: L) (fun x => B ->ᵐ f x)) :
    π₂ᵐ ∘ᵐ (genMedᵐ Π) ≡ π₂ᵐ Π.
  Proof.
    apply eqextᵐ; intros; simpl; reflexivity.
  Qed.

  Lemma genProdEqᵐ {L} (P Q : genProd L f) :
    (forall a, forall (v : Var L a), genProdProjᵐ v P ≡ genProdProjᵐ v Q) -> P ≡ Q.
    induction L; simpl in *; intros;destruct P; destruct Q.
    + reflexivity.
    + specialize IHL with g0 g1. rewrite IHL.
      * specialize H1 with a (V0 _ _). simpl in H1. rewrite H1; reflexivity.
      * intros. specialize H1 with a0 (VS _ _ _ v). simpl in *. auto.
  Qed.

  Lemma genProdProjᵐ_genMed {a L} {F : forall x, g x  -> (B ->ᵐ f x)}
        (v : Var L a) (P : genProd L g) b :
    (genProdProjᵐ v)  (genMedᵐ (genProdTrans F P) b) ≡ F a (genProdProj v P) b.
  Proof.
    induction L; simpl in *; dependent destruction v.
    + reflexivity.
    + destruct P; simpl.
      unfold uncurry, med, tens, compose in *; simpl in *.
      apply IHL.
  Qed.

  Lemma genProdProjᵐ_genProdTrans {L a} {F : forall x, h x ->ᵐ f x}
        (v : Var L a) (P : genProd L h):
    genProdProjᵐ v (genProdTransᵐ F P) = F a (genProdProj v P).
  Proof.
    induction L; simpl in *; dependent destruction v.
    + reflexivity.
    + destruct P; simpl.
      unfold uncurry, med, tens, compose in *; simpl in *.
    apply IHL.
  Qed.
End GenProdPropsPOS.

Section GenProdωCPO.
  Context {A} {f : A -> Type} `{forall a, POSet (f a)} `{forall a, ωCPO (f a)}.

  Global Instance ωCPOgenProd (L : list A) : ωCPO (genProd L f).
  Proof.
    induction L; simpl; [exact UnitωCPO | exact ProdωCPO].
  Defined.
End GenProdωCPO.

Section GenProdDefs₂.
  Context {A B} {f : A -> Type} {g : A -> Type}
          `{forall a, POSet (f a)} `{forall a, POSet (g a)}
          `{POSet B} `{forall a, ωCPO (f a)} `{forall a, ωCPO (g a)} `{ωCPO B}.

  Definition πg₁ᶜ {a L L₀} (HEqL: a :: L = L₀) (Π : genProd L₀ (fun x => B ->ᶜ f x)) :
    genProd L (fun x => B ->ᶜ f x).
  subst; simpl in *; exact (π₁ᶜ Π).
  Defined.

  Definition πg₂ᶜ {a L L₀} (HEqL: a :: L = L₀) (Π : genProd L₀ (fun x => B ->ᶜ f x)) : B ->ᶜ f a.
    subst; simpl in *. exact (π₂ᶜ Π).
  Defined.

  Program Fixpoint genMedᶜ {L} (Π : genProd L (fun x => B ->ᶜ f x)) :  B ->ᶜ genProd L f :=
    match L with
    | nil => constᶜ ()
    | a :: L' => medᶜ (genMedᶜ (L:=L') (πg₁ᶜ _ Π)) (πg₂ᶜ _ Π)
    end.

  Program Fixpoint genProdTransᶜ{L}
          (F : forall x, f x  ->ᶜ g x) : genProd L f ->ᶜ genProd L g :=
    match L with
    | nil => constᶜ ()
    | a :: L' => genProdTransᶜ (L:=L') F ×ᶜ (F a)
    end.

  Program Fixpoint genProdProjᶜ {L a} (v : Var L a) : genProd L f ->ᶜ f a :=
    match v with
    | V0 _ _ => π₂ᶜ
    | VS _ _ _ v' => (genProdProjᶜ v') ∘ᶜ π₁ᶜ
    end.

  Global Instance monotone_genMedᶜ (L : list A) F: Proper (preo ++> preo) (genMedᶜ (L:=L) F).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.

  Global Instance equiv_genMedᶜ (L : list A) F: Proper (equiv ++> equiv) (genMedᶜ (L:=L) F).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.
  Global Instance monotone_genProdTransᶜ (L : list A) F :
    Proper (preo ++> preo) (genProdTransᶜ (L:=L) F).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.
  Global Instance equiv_genProdTransᶜ (L : list A) F :
    Proper (equiv ++> equiv) (genProdTransᶜ (L:=L) F).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.
  Global Instance equiv_genProdProjᶜ (L : list A) (a:A) v :
    Proper (equiv ++> equiv) (genProdProjᶜ (L:=L) (a:=a) v).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.
  Global Instance monotone_genProdProjᶜ (L : list A) a v :
    Proper (preo ++> preo) (genProdProjᶜ (L:=L) (a:=a) v).
  Proof.
    intros b₁ b₂ LEb. rewrite LEb. reflexivity.
  Defined.

End GenProdDefs₂.

Section GenProdPropsωCPO.
  Context {A B}  {f g h : A -> Type}
          `{forall a, POSet (f a)}`{forall a, POSet (h a)} `{poB : POSet B}
          `{forall a, ωCPO (f a)} `{forall a, ωCPO (h a)} `{ωCPO B}.

  (** genProd L f is a generalized product *)
  Lemma genMedᶜ₀ (Π : genProd nil (fun x => B ->ᶜ f x)) :
    genMedᶜ Π ≡ constᶜ ().
  Proof.
    apply uniq_termᵐ.
  Qed.

  Lemma genMedᶜ₁ {a L} (Π : genProd (a :: L) (fun x => B ->ᶜ f x)) :
    π₁ᶜ ∘ᶜ (genMedᶜ Π) ≡ genMedᶜ (π₁ᶜ Π).
  Proof.
    simpl in *.
    apply eqextᵐ. intro b; destruct Π. simpl.
    reflexivity.
  Qed.

  Lemma genMedᶜ₂ {a L} (Π : genProd (a :: L) (fun x => B ->ᶜ f x)) :
    π₂ᶜ ∘ᶜ (genMedᶜ Π) ≡ π₂ᶜ Π.
  Proof.
    apply eqextᵐ; intros; simpl; reflexivity.
  Qed.

  Lemma genProdEqᶜ {L} (P Q : genProd L f) :
    (forall a, forall (v : Var L a), genProdProjᶜ v P ≡ genProdProjᶜ v Q) -> P ≡ Q.
    induction L; simpl in *; intros;destruct P; destruct Q.
    + reflexivity.
    + specialize IHL with g0 g1. rewrite IHL.
      * specialize H4 with a (V0 _ _). simpl in H4. rewrite H4; reflexivity.
      * intros. specialize H4 with a0 (VS _ _ _ v). simpl in *. auto.
  Qed.

  Lemma genProdProjᶜ_genMed {a L} {F : forall x, g x -> (B ->ᶜ f x)}
        (v : Var L a) (P : genProd L g) (b : B):
    (genProdProjᶜ v) (genMedᶜ (genProdTrans F P) b) ≡ F a (genProdProj v P) b.
   Proof.
    induction L; simpl in *; dependent destruction v.
    + reflexivity.
    + destruct P; simpl.
      unfold uncurry, med, tens, compose in *; simpl in *.
      apply IHL.
   Qed.

   Lemma genProdProjᶜ_genProdTrans {L a} {F : forall x, h x ->ᶜ f x}
         (v : Var L a) (P : genProd L h):
     genProdProjᶜ v (genProdTransᶜ F P) = F a (genProdProj v P).
   Proof.
     induction L; simpl in *; dependent destruction v.
     + reflexivity.
     + destruct P; simpl.
    unfold uncurry, med, tens, compose in *; simpl in *.
    apply IHL.
   Qed.

End GenProdPropsωCPO.
